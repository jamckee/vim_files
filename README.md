# vim_files  
  
.gdbinit - GDB Dashboard  
https://github.com/cyrus-and/gdb-dashboard  
  
southernlights.vim - vim color scheme  
https://github.com/jalvesaq/southernlights  
  
.vimrc - Originally made by Christopher Smith  
  
.astylerc - config file for astyle  
  
  
download via linux:  
mkdir -p ~/.vim/colors  
wget -O ~/.vim/colors/southernlights.vim https://gitlab.com/jamckee/vim_files/raw/master/southernlights.vim  
wget -O ~/.astylerc https://gitlab.com/jamckee/vim_files/raw/master/.astylerc  
wget -O ~/.gdbinit https://gitlab.com/jamckee/vim_files/raw/master/.gdbinit  
wget -O ~/.vimrc https://gitlab.com/jamckee/vim_files/raw/master/.vimrc  