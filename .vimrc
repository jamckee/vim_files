set nocompatible
colorscheme southernlights
syntax on "Enable Syntax Processing
filetype indent on "Load filetype specific indent files
autocmd FileType make setlocal noexpandtab


set tabstop=2	"Number of Visual Spaces per Tab
set softtabstop=2 "Number of Spaces in Tab when editing
set expandtab "Tabs are Spaces
set number "show line numbers
set showcmd "show command in bottom bar
set wildmenu "Visual Autocomplete for Command Menu
set lazyredraw "redraw only when we need to
set showmatch "highlight matching [{()}]
set incsearch "search as characters are entered
set hlsearch "highlight matches
"Turn off search highlight
nnoremap <leader><space> nohlsearch<CR>
set foldenable "enable folding
set foldlevelstart=10 "open most folds by default
set foldnestmax=10 "10 Nested fold max
"Space open Close folds
nnoremap <space> za
set foldmethod=indent "fold based on indent level
"Move vertically by visual line
nnoremap j gj
nnoremap k gk

"Move to beginning/end of line
nnoremap B ^
nnoremap E $

"$/^ doesn't do anything
nnoremap $ <nop>
nnoremap ^ <nop>

"Highlight last inserted text
nnoremap gV `[v`]

let mapleader="," "Leader is comma

"jk is escape
inoremap jk <esc>

" toggle gundo
nnoremap <leader>u :GundoToggle<CR>

